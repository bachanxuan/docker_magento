# Create user and grant rights
FLUSH PRIVILEGES;
CREATE USER 'magento'@'%' IDENTIFIED BY 'password';
GRANT ALL ON * TO 'magento'@'%';

CREATE USER 'admin'@'%' IDENTIFIED BY 'password';
GRANT ALL ON * TO 'admin'@'%';
